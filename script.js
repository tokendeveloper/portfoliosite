$(document).ready(function() {
	$("#toTop").click(function() {
		scrollToAnchor('toTop');
	});

	$("#toAbout").click(function(){
		scrollToAnchor('toAbout');
	});

	$("#toStack").click(function(){
		scrollToAnchor('toStack');
	});

	$("#toProjects").click(function(){
		scrollToAnchor('toProjects');
	});

	$("#toContact").click(function(){
		scrollToAnchor('toContact');
	});

	$("#toScrollDown").click(function(){
		scrollToAnchor('toAbout');
	});
	
	//Retrieved from https://stackoverflow.com/questions/5347357/jquery-get-selected-element-tag-name
	//Use jquery to do a scroll animation.
	//scrollToAnchor takes a parameter "titleattr"(titile attribute).
	//variable anchor is stored as jquery's attribute selector, which selects anchor tag's name attribute (in this form: $("[attribute~='value']") ).
	//$('html, body') targets the DOM object "html" and "body", and applies the animate method.
	//the method takes an object key-value pair and string value for speed of scroll animation as the parameters.
	//the object key is "scrollTop, and it takes the value pair "anchor.offset()",
	//which is a method used to offset the page to the defined position (in this case, it searches for the anchorr tag).
	//function is called on the jquery method ".click", which takes a call-back function as param to call the scrollToAnchor funciton.
	function scrollToAnchor(titleattr){
		var anchor = $("a[title='"+ titleattr +"']");
		$('html,body').animate({scrollTop: anchor.offset().top},500);
	}


	const hamMenu = document.getElementById('ham-menu');
	const navList = document.getElementById('navList');

	//Add or remove 'responsive' to navigation link list. 
	hamMenu.onclick = function (){
		if(navList.className !== 'responsive'){
			navList.className += 'responsive';

		} else if (navList.className === 'responsive'){
			navList.className = '';
		} 
	};

})



